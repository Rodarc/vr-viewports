extends "res://DrawerClass.gd"

export (NodePath) var button_path
onready var button = get_node(button_path)

var rng = RandomNumberGenerator.new()

class DrawLines:
#	var amount_of_lines = 1000
	var width = 8
	var max_distance = 50
	var min_distance = 30
	func drawLine():
		print('PA, PB, Grosor,Color')
	
	func drawLines():
		print('Array de posiciones 2n, array de grosores n, array de colores n')
		
var lineDrawer = DrawLines.new()
var direction = [1,-1]
func _draw():
	rng.randomize()
	var beginning_x
	var beginning_y
	draw_circle(Vector2(x_origin,y_origin),20,ColorN('Red'))
	for i in amount_of_lines:
#		beginning_x = rng.randi_range(0, lineDrawer.weight)
#		beginning_y = rng.randi_range(0, lineDrawer.height)
		beginning_x = x_origin + rng.randi_range(x_negative, x_positive)
		beginning_y = y_origin - rng.randi_range(y_negative, y_positive)
		draw_line(
			Vector2(beginning_x,beginning_y), 
			Vector2(beginning_x + rng.randi_range(lineDrawer.min_distance, lineDrawer.max_distance)*direction[rng.randi_range(0,1)],
			beginning_y + rng.randi_range(lineDrawer.min_distance, lineDrawer.max_distance)*direction[rng.randi_range(0,1)]),
			Color(rng.randf_range(0,1),rng.randf_range(0,1),rng.randf_range(0,1)),
			lineDrawer.width,
			false)
		
func _ready():
	print('Everything is fine')


func _on_runLines_pressed():
	amount_of_lines = $CanvasLayer/runLines/SpinBox2.value
	update()

